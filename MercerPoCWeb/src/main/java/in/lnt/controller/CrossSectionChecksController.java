package in.lnt.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.general.JsonUtils;
import in.lnt.validations.PerFormL3BValidations;

/**
 * Handles requests for the Level 3B
 */
@Controller
public class CrossSectionChecksController {

	private static final Logger logger = LoggerFactory.getLogger(CrossSectionChecksController.class);
	private static ObjectMapper mapper = new ObjectMapper();

	/**
	 * Upload multiple file using Spring Controller
	 * 
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	@RequestMapping(value = "/crossSectionChecks", method = RequestMethod.POST,
			consumes = "application/json",produces="application/json")
	public @ResponseBody void crossSectionChecksHandler(@RequestBody String validationData, HttpServletRequest request,
			HttpServletResponse response){
		performL3BValidations(validationData, request, response);
	}


	private void performL3BValidations(String validationData, HttpServletRequest request,
			HttpServletResponse response) {
		long startTime = System.currentTimeMillis();
		ObjectNode errNode = mapper.createObjectNode();
		
		Boolean isMDARequest = Boolean.valueOf(String.valueOf(request.getAttribute(Constants.ISMDAREQUEST)));
		try {
			JsonNode l3BOutput = PerFormL3BValidations.performL3B(validationData, isMDARequest);
			if (l3BOutput != null) {
				downLoadData(response, l3BOutput.toString().getBytes(Constants.UTF_8), 200);
			}
		}  catch (CustomStatusException cse) {
			errNode = JsonUtils.updateErrorNode(cse,errNode);
			try {
				downLoadData(response, errNode.toString().getBytes(Constants.UTF_8), cse.getHttpStatus());
			} catch (UnsupportedEncodingException uee) {
				logger.error("UnsupportedEncodingException : {}",uee.getMessage());	
			}
		} catch (Exception e) {
			logger.error("Excpetion is {}",e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			try {
				downLoadData(response, errNode.toString().getBytes(Constants.UTF_8), Constants.HTTP_STATUS_500);
			} catch (UnsupportedEncodingException uee) {
				logger.error("Unsupported Encoding Exception :{} ",uee.getMessage());	
			}
		}finally {
			logger.info(" Exiting from  crossSectionChecks method for json body {}",(System.currentTimeMillis() - startTime));	
		}
	}


		
	
	
	/**
	 * @author Jwala Pradhan
	 * @param validationData
	 * @param request
	 * @param response
	 * @throws Exception
	 * @story PE:5414
	 */
	@RequestMapping(value = "/performDefaultActions", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody void mercerDefinedActionsHandler(@RequestBody String validationData,
			HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute(Constants.ISMDAREQUEST, true);
		performL3BValidations(validationData, request, response);
	}	
	
	void downLoadData(HttpServletResponse response, byte[] bs, int status) {
		InputStream is;
		ObjectNode errNode = null;
		try {
			is = new ByteArrayInputStream(bs);
			response.setStatus(status);
			// MIME type of the file
			response.setContentType("application/json");
			// Response header
			response.setHeader("Content-Disposition", "attachment; filename=\"output.json\"");

			// Read from the file and write into the response
			OutputStream os = response.getOutputStream();
			byte[] buffer = new byte[1024];
			int len;
			while ((len = is.read(buffer)) != -1) {
				os.write(buffer, 0, len);
			}
			os.flush();
			os.close();
			is.close();
		} catch (Exception e) {
			logger.error("Exception occured in downloadData..,{}",e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			try {
				downLoadData(response, errNode.toString().getBytes(Constants.UTF_8), Constants.HTTP_STATUS_500);
			} catch (UnsupportedEncodingException uee) {
				logger.error("UnsupportedEncodingException : {}",uee.getMessage());	
			}
			
		}
	}
	
}
