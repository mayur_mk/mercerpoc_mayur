package com.lti.mosaic.parser.exception;

/**
 * Licensed Information -- need to work on this 
 */

/**
 *  Wraper Expception for all Exception 
 * <p/>
 * 
 * @author Piyush
 * @since 1.0
 * @version 1.0
 */


public class ExpressionEvaluatorException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExpressionEvaluatorException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public ExpressionEvaluatorException(String message) {
		super(message);
	}
}
